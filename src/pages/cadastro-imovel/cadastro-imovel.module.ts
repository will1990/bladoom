import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastroImovelPage } from './cadastro-imovel';

@NgModule({
  declarations: [
    CadastroImovelPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastroImovelPage),
  ],
})
export class CadastroImovelPageModule {}
