import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CompradorPage } from './comprador';

@NgModule({
  declarations: [
    CompradorPage,
  ],
  imports: [
    IonicPageModule.forChild(CompradorPage),
  ],
})
export class CompradorPageModule {}
