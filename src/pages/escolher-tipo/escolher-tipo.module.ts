import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolherTipoPage } from './escolher-tipo';

@NgModule({
  declarations: [
    EscolherTipoPage,
  ],
  imports: [
    IonicPageModule.forChild(EscolherTipoPage),
  ],
})
export class EscolherTipoPageModule {}
