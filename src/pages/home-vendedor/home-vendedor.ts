import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the HomeVendedorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-vendedor',
  templateUrl: 'home-vendedor.html',
})
export class HomeVendedorPage {
  bgImage:any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.bgImage = '/assets/imgs/img_imovel.jpg';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeVendedorPage');
  }

}
