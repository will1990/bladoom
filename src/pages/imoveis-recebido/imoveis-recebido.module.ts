import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImoveisRecebidoPage } from './imoveis-recebido';

@NgModule({
  declarations: [
    ImoveisRecebidoPage,
  ],
  imports: [
    IonicPageModule.forChild(ImoveisRecebidoPage),
  ],
})
export class ImoveisRecebidoPageModule {}
