import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImovelRecebidoPage } from './imovel-recebido';

@NgModule({
  declarations: [
    ImovelRecebidoPage,
  ],
  imports: [
    IonicPageModule.forChild(ImovelRecebidoPage),
  ],
})
export class ImovelRecebidoPageModule {}
