import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaOfertaCompradorPage } from './nova-oferta-comprador';

@NgModule({
  declarations: [
    NovaOfertaCompradorPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaOfertaCompradorPage),
  ],
})
export class NovaOfertaCompradorPageModule {}
