import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaOfertaPage } from './nova-oferta';

@NgModule({
  declarations: [
    NovaOfertaPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaOfertaPage),
  ],
})
export class NovaOfertaPageModule {}
