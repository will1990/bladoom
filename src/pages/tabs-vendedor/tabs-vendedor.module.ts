import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsVendedorPage } from './tabs-vendedor';

@NgModule({
  declarations: [
    TabsVendedorPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsVendedorPage),
  ],
})
export class TabsVendedorPageModule {}
