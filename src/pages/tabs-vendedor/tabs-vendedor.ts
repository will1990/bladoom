import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs-vendedor',
  templateUrl: 'tabs-vendedor.html',
})
export class TabsVendedorPage {
  Pagina1: any = 'HomePage';
  Pagina2: any = 'HomeVendedorPage';
  Pagina3: any = 'HomePage';
  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.myIndex = navParams.data.tabIndex || 1;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsVendedorPage');
  }

}
