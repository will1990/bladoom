import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  Pagina1: any = 'HomePage';
  Pagina2: any = 'HomeUsuarioPage';
  Pagina3: any = 'HomePage';
  myIndex: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.myIndex = navParams.data.tabIndex || 1;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  returnNav(){
    this.navCtrl.pop();
  }

}
